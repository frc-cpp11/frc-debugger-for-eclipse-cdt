package edu.wpi.first.frc_debugger.terminal;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.rse.services.clientserver.messages.SystemMessage;
import org.eclipse.rse.services.clientserver.messages.SystemMessageException;
import org.eclipse.rse.services.terminals.AbstractTerminalService;
import org.eclipse.rse.services.terminals.ITerminalShell;

import edu.wpi.first.frc_debugger.terminal.connector.NetConsoleConnectorService;

public class NetConsoleService extends AbstractTerminalService {
	private NetConsoleConnectorService c;

	public NetConsoleService(NetConsoleConnectorService c) {
		this.c = c;
	}

	public ITerminalShell launchTerminal(String ptyType, String encoding,
			String[] environment, String initialWorkingDirectory,
			String commandToRun, IProgressMonitor monitor)
			throws SystemMessageException {
		NetConsoleShell hostShell;
		try {
			hostShell = new NetConsoleShell(c.getNetConsole(monitor),
					initialWorkingDirectory, commandToRun);
			return hostShell;
		} catch (Exception e) {
			throw new SystemMessageException(new SystemMessage("", "", "",
					'\0', "", ""));
		}
	}

	public String getName() {
		return "Netconsole cRIO Terminal";
	}

	public String getDescription() {
		return "Uses the Netconsole to send and recieve information from the cRIO";
	}
}
