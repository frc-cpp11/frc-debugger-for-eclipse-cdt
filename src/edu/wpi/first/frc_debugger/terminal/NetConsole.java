package edu.wpi.first.frc_debugger.terminal;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;

public class NetConsole {
	private static final int PORT_IN = 6666;
	private static final int PORT_OUT = 6668;
	private static final int RECIEVE_BUFFER_SIZE = 4096;
	private final InetAddress destinationAddress;
	private DatagramSocket sockIn;
	private DatagramSocket sockOut;

	public NetConsole(String ip) throws IOException {
		destinationAddress = InetAddress.getByName(ip);
		sockIn = new DatagramSocket(PORT_IN);
		sockOut = new DatagramSocket(PORT_OUT);
		sockOut.setBroadcast(true);
	}

	public void send(String s) throws IOException {
		byte[] raw = s.concat("\r\n").getBytes();
		DatagramPacket pack = new DatagramPacket(raw, raw.length);
		pack.setSocketAddress(new InetSocketAddress(destinationAddress,
				PORT_OUT));
		sockOut.send(pack);
	}

	public String read() throws IOException {
		DatagramPacket read = new DatagramPacket(new byte[RECIEVE_BUFFER_SIZE],
				RECIEVE_BUFFER_SIZE);
		sockIn.receive(read);
		return new String(read.getData(), read.getOffset(), read.getLength());
	}

	public boolean isConnected() {
		return sockIn.isConnected() || sockOut.isConnected();
	}

	public void disconnect() {
		if (sockIn.isConnected()) {
			sockIn.disconnect();
		}
		if (sockOut.isConnected()) {
			sockOut.disconnect();
		}
	}
}
