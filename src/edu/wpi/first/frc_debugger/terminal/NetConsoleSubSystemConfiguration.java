/*******************************************************************************
 *  Copyright (c) 2006, 2008 IBM Corporation and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Initial Contributors:
 *  The following IBM employees contributed to the Remote System Explorer
 *  component that contains this file: David McKnight, Kushal Munir, 
 *  Michael Berger, David Dykstal, Phil Coulthard, Don Yantzi, Eric Simpson, 
 *  Emily Bruner, Mazen Faraj, Adrian Storisteanu, Li Ding, and Kent Hawley.
 * 
 *  Contributors:
 *  Martin Oberhuber (Wind River) - Adapted template for ssh service.
 *  Sheldon D'souza  (Celunite)   - Adapted template for telnet service
 *  Anna Dushistova  (MontaVista) - [240523] [rseterminals] Provide a generic adapter factory that adapts any ITerminalService to an IShellService
 *******************************************************************************/
package edu.wpi.first.frc_debugger.terminal;

import org.eclipse.rse.core.model.IHost;
import org.eclipse.rse.core.subsystems.IConnectorService;
import org.eclipse.rse.core.subsystems.ISubSystem;
import org.eclipse.rse.services.shells.IHostShell;
import org.eclipse.rse.services.shells.IShellService;
import org.eclipse.rse.subsystems.shells.core.subsystems.IRemoteCmdSubSystem;
import org.eclipse.rse.subsystems.shells.core.subsystems.servicesubsystem.IServiceCommandShell;
import org.eclipse.rse.subsystems.shells.core.subsystems.servicesubsystem.ShellServiceSubSystem;
import org.eclipse.rse.subsystems.shells.core.subsystems.servicesubsystem.ShellServiceSubSystemConfiguration;

import edu.wpi.first.frc_debugger.terminal.connector.NetConsoleConnectorService;
import edu.wpi.first.frc_debugger.terminal.connector.NetConsoleConnectorServiceManager;

public class NetConsoleSubSystemConfiguration extends
		ShellServiceSubSystemConfiguration {

	public NetConsoleSubSystemConfiguration() {
		super();
	}

	public boolean isFactoryFor(Class subSystemType) {
		boolean isFor = ShellServiceSubSystem.class.equals(subSystemType);
		return isFor;
	}

	public ISubSystem createSubSystemInternal(IHost host) {
		IConnectorService connectorService = (NetConsoleConnectorService) getConnectorService(host);
		ISubSystem subsys = new ShellServiceSubSystem(host, connectorService,
				createShellService(host));
		return subsys;
	}

	public IShellService createShellService(IHost host) {
		NetConsoleConnectorService cserv = (NetConsoleConnectorService) getConnectorService(host);
		return (IShellService) (new NetConsoleService(cserv))
				.getAdapter(IShellService.class);
	}

	public IConnectorService getConnectorService(IHost host) {
		return NetConsoleConnectorServiceManager.getInstance()
				.getConnectorService(host, NetConsoleService.class);
	}

	public void setConnectorService(IHost host,
			IConnectorService connectorService) {
		NetConsoleConnectorServiceManager.getInstance().setConnectorService(
				host, NetConsoleService.class, connectorService);
	}

	public Class getServiceImplType() {
		return NetConsoleService.class;
	}

	public IServiceCommandShell createRemoteCommandShell(
			IRemoteCmdSubSystem cmdSS, IHostShell hostShell) {
		return new NetconsoleCommandShell(cmdSS, hostShell);
	}

}
