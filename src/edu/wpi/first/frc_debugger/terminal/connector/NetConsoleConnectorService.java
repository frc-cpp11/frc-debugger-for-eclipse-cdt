/*******************************************************************************
 * Copyright (c) 2006, 2009 Wind River Systems, Inc. and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * Martin Oberhuber (Wind River) - initial API and implementation
 * David Dykstal    (IBM)        - [168977] refactoring IConnectorService and ServerLauncher hierarchies
 * Sheldon D'souza  (Celunite)   - adapted from SshConnectorService
 * Martin Oberhuber (Wind River) - apply refactorings for StandardConnectorService
 * Martin Oberhuber (Wind River) - [178606] fix endless loop in readUntil()
 * Sheldon D'souza  (Celunite)   - [186536] login and password should be configurable
 * Sheldon D'souza  (Celunite)   - [186570] handle invalid user id and password more gracefully
 * Martin Oberhuber (Wind River) - [187218] Fix error reporting for connect()
 * Sheldon D'souza  (Celunite)   - [187301] support multiple telnet shells
 * Sheldon D'souza  (Celunite)   - [194464] fix create multiple telnet shells quickly
 * Martin Oberhuber (Wind River) - [186761] make the port setting configurable
 * David McKnight   (IBM)        - [216252] [api][nls] Resource Strings specific to subsystems should be moved from rse.ui into files.ui / shells.ui / processes.ui where possible
 * David McKnight   (IBM)        - [220547] [api][breaking] SimpleSystemMessage needs to specify a message id and some messages should be shared
 * Anna Dushistova  (MontaVista) - [240523] [rseterminals] Provide a generic adapter factory that adapts any ITerminalService to an IShellService
 * Anna Dushistova  (MontaVista) - [198819] [telnet] TelnetConnectorService does not send CommunicationsEvent.BEFORE_CONNECT
 *******************************************************************************/
package edu.wpi.first.frc_debugger.terminal.connector;

import java.io.IOException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.rse.core.model.IHost;
import org.eclipse.rse.core.subsystems.BasicConnectorService;

import edu.wpi.first.frc_debugger.terminal.NetConsole;

public class NetConsoleConnectorService extends BasicConnectorService {
	private NetConsole netConsole;

	public NetConsoleConnectorService(IHost host) throws IOException {
		super("NetConsole Connector Service", "Provides NetConsoles", host, -1);
		netConsole = new NetConsole(host.getHostName());
	}

	public NetConsole getNetConsole(IProgressMonitor monitor) throws Exception {
		if (!netConsole.isConnected()) {
			netConsole = new NetConsole(getHost().getHostName());
		}
		return netConsole;
	}

	@Override
	public boolean isConnected() {
		return netConsole.isConnected();
	}

	@Override
	protected void internalConnect(IProgressMonitor monitor) throws Exception {
		if (!netConsole.isConnected()) {
			netConsole = new NetConsole(getHost().getHostName());
		}
	}

	@Override
	protected void internalDisconnect(IProgressMonitor monitor)
			throws Exception {
		if (netConsole.isConnected()) {
			netConsole.disconnect();
		}
	}
}
