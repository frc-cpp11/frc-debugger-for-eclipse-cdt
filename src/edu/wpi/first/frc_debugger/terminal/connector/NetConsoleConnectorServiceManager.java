package edu.wpi.first.frc_debugger.terminal.connector;

import java.io.IOException;

import org.eclipse.rse.core.model.IHost;
import org.eclipse.rse.core.subsystems.AbstractConnectorServiceManager;
import org.eclipse.rse.core.subsystems.ISubSystem;
import org.eclipse.rse.subsystems.shells.core.subsystems.servicesubsystem.ShellServiceSubSystem;

import edu.wpi.first.frc_debugger.terminal.NetConsoleSubSystemConfiguration;

public class NetConsoleConnectorServiceManager extends
		AbstractConnectorServiceManager {
	private static NetConsoleConnectorServiceManager m;
	
	public static NetConsoleConnectorServiceManager getInstance() {
		if (m==null) {
			m = new NetConsoleConnectorServiceManager();
		}
		return m;
	}
	public NetConsoleConnectorService createConnectorService(IHost i) {
		try {
			return new NetConsoleConnectorService(i);
		} catch (IOException e) {
			return null;
		}
	}
	@Override
	public boolean sharesSystem(ISubSystem s) {
		return s.getClass().equals(ShellServiceSubSystem.class);
	}
	@Override
	public Class getSubSystemCommonInterface(ISubSystem subsystem) {
		return NetConsoleSubSystemConfiguration.class;
	}
}
