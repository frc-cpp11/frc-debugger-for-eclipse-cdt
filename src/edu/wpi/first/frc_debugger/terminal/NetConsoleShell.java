/*******************************************************************************
 * Copyright (c) 2008, 2012 Wind River Systems, Inc. and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * Martin Oberhuber (Wind River) - initial API and implementation
 * Anna Dushistova  (MontaVista) - [170910] Integrate the TM Terminal View with RSE
 * Martin Oberhuber (Wind River) - [227320] Fix endless loop in TelnetTerminalShell
 * Anna Dushistova  (MontaVista) - [240523] [rseterminals] Provide a generic adapter factory that adapts any ITerminalService to an IShellService
 * Martin Oberhuber (Wind River) - [267402] [telnet] "launch shell" takes forever
 * Anna Dushistova  (MontaVista) - [267474] [rseterminal][telnet] Notify the remote when terminal window size changes
 *******************************************************************************/

package edu.wpi.first.frc_debugger.terminal;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.eclipse.rse.services.terminals.AbstractTerminalShell;

public class NetConsoleShell extends AbstractTerminalShell {
	private NetConsole netConsole;

	public NetConsoleShell(NetConsole c, String initialWorkingDirectory,
			String commandToRun) throws IOException {
		this.netConsole = c;
		if (c.isConnected()) {
			c.send("cd " + initialWorkingDirectory);
			c.send(commandToRun);
		}
	}

	@Override
	public OutputStream getOutputStream() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public InputStream getInputStream() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isActive() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void exit() {
		// TODO Auto-generated method stub

	}
}
